# Trivia
![Dependencies](https://img.shields.io/david/antonderegt/trivia-game)
![Size](https://img.shields.io/github/languages/code-size/antonderegt/trivia-game)
![Last commit](https://img.shields.io/github/last-commit/antonderegt/trivia-game)
![Issues](https://img.shields.io/github/issues/antonderegt/trivia-game)
![License](https://img.shields.io/github/license/antonderegt/trivia-game?label=license)
![Pipeline](https://gitlab.com/antonderegt/trivia-game/badges/master/pipeline.svg)

## Demo
Play the game [here](https://trivia-game-anton.herokuapp.com/).

## Game Instructions
### Setup
1. Enter the number of questions you want to answer
2. Set the difficulty level
3. Select a category you know a lot about
4. Click 'Start Game' or press enter

### Answering questions
Answer the questions by clicking the buttons

### Results
On the last page you can view your score and how to answered each question. If you're up for another game with the same settings, just click 'Play Again'. If you want to return home or play the game with different settings, just click 'Home'.

## Prerequisites
To install, run and build: [Node.js](https://nodejs.org/en/)  
To deploy (optionally): [Heroku](https://heroku.com/)  

## CI/DI
The game is deployed on [Heroku](https://heroku.com/). On each merge with the master branch the deploy pipeline kicks in automatically. See this article by  [Oliver Kocsis](https://medium.com/@oliver.kocsis/deploy-app-to-heroku-from-gitlab-209983635f3) on how to set it up.

### Manual deploy
You can also manually deploy the app. After you have created a project on [Heroku](https://heroku.com/) deploy it with this command:
```
git push heroku master
```

To see the latest logs:
```
heroku logs --tail
```

View your app:
```
heroku open
```

## Project setup
Clone project and install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Wireframe
See the wireframe [here](trivia-wireframe.pdf).

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Inspiration
For the color system I was inspired by [Material Design](https://material.io/design/color/the-color-system.html#color-usage-and-palettes).